for (var key in window.articles) {
    idx.add({
	'id': key,
	'title': window.store[key].title,
	'author': window.store[key].author,
	'category': window.store[key].category,
	'content': window.store[key].content
    });
    
    var results = idx.search(searchTerm); // Get lunr to perform a search
    displaySearchResults(results, articles, searchTerm); // We'll write this in the next section
}

function displaySearchResults(results, store, term) {
    var searchResults = document.getElementById('search-results');
    if(!term) term = '';

    if (results.length) { // Are there any results?
	var appendString = '';
	
	for (var i = 0; i < results.length; i++) {  // Iterate over the results
	    var item = store[results[i].ref];
	    appendString += '<a class="card" href="' + item.url + '">';
	    appendString += '<h2>' + highlight(item.title, term, false) + '</h2>';
	    appendString += '<p>' + highlight(item.content, term, true) + '...</p>';
	    
	    appendString += '</a>';
	}	
	searchResults.innerHTML = appendString;
    } else {
	searchResults.innerHTML = '<p> No Results found </p>';
    }
}
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');

    for (var i = 0; i < vars.length; i++) {
	var pair = vars[i].split('=');

	if (pair[0] === variable) {
	    return decodeURIComponent(pair[1].replace(/\+/g, '%20'));
	}
    }
}
function highlight(text, term, truncate){
    var i_match = text.toLowerCase().indexOf(term.toLowerCase());
    if(i_match !== -1){
	var text = text.substring(0, i_match)+ '<span class="highlight-text">'+ text.substring(i_match, i_match+term.length) + '</span>' + text.substring(i_match+term.length, text.length)
    }
    
    if(truncate){
	var max_length = 150;
	return i_match - max_length > 0 ? text.substring(i_match - 20, i_match + 130 ) : text.substring(0, 150);
    }
    return text;
}

var searchTerm = getQueryVariable('q');
if (searchTerm) {
    document.getElementById('search-box').setAttribute("value", searchTerm);

    // Initalize lunr with the fields it will be searching on. I've given title
    // a boost of 10 to indicate matches on this field are more important.
    var idx = lunr(function () {
	this.field('id');
	this.field('title', { boost: 5 });
	this.field('tags', { boost: 10 });
	this.field('author');
	this.field('category');
	this.field('content');
    });

    for (var key in window.store) { // Add the data to lunr
	idx.add({
	    'id': key,
	    'title': window.store[key].title,
	    'tags': window.store[key].tags,
	    'author': window.store[key].author,
	    'category': window.store[key].category,
	    'content': window.store[key].content
	});

	var results = idx.search(searchTerm); // Get lunr to perform a search
	displaySearchResults(results, window.store, searchTerm); // We'll write this in the next section
    }
}


